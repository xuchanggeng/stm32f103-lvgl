#include <stdio.h>
#include <string.h>
#include "ScanDir.h"
#include "ff.h"

static FATFS   scan_fs;             /* Filesystem object */
static FIL     scan_file;           /* 文件对象 */
static FRESULT scan_res;            /* 文件操作结果 */
static UINT    br;

static uint16_t dir_sz = 0, file_sz = 0;
static char dir_path[10*256];
static char file_path[10*256];

void ScanTest(void)
{
    static char buff[256];
    FRESULT res;
    if(ScanMount()==FR_OK)
    {
        strcpy(buff, "/");
        res = scan_files(buff);
        
        printf("Dir: \n\r%s\n\r",dir_path);
        printf("File: \n\r%s\n\r",file_path);
    }
}

FRESULT ScanMount(void)
{
    scan_res = f_mount(&scan_fs, "0:", 1);
    if(scan_res == FR_OK)
    {
        printf("FATFS mount succeed\n\r");
    }
    else if(scan_res == FR_NO_FILESYSTEM)
    {
        printf("SD not inited\n\r");
    }
    else
    {
        printf("FATFS mount Failed\n\r");
        scan_res = f_mount(&scan_fs, "0:", 0);
    }
    return scan_res;
}

FRESULT scan_files (char* path)
{
    FRESULT res;
    DIR dir;
    UINT i;
    static FILINFO fno;

    res = f_opendir(&dir, path);                       /* Open the directory */
    if (res == FR_OK) 
    {
        for (;;) 
        {
            res = f_readdir(&dir, &fno);                   /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
            if (fno.fattrib & AM_DIR) 
            {                    /* It is a directory */
                i = strlen(path);
                sprintf(&path[i], "/%s", fno.fname);
                sprintf(&dir_path[dir_sz], "%s\n\r", path);
                dir_sz += (strlen(path)+2);
                res = scan_files(path);                    /* Enter the directory */
//                printf("Dir:%s\n\r", path);
                if (res != FR_OK) break;
                path[i] = 0;
            } 
            else 
            {                                       /* It is a file. */
//                printf("%s/%s\n\r", path, fno.fname);
                sprintf(&file_path[file_sz], "%s/%s\n\r", path, fno.fname);
                file_sz += (strlen(path) + 1 + strlen(fno.fname) +2);
            }
        }
        f_closedir(&dir);
    }

    return res;
}

