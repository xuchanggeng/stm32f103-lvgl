#ifndef __SCAN_DIR_H
#define __SCAN_DIR_H

#include "main.h"
#include "ff.h"

extern void ScanTest(void);
extern FRESULT ScanMount(void);
extern FRESULT scan_files (char* path);

#endif
