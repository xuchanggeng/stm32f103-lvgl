#ifndef LV_100ASK_STM32_TOOL_FILE_BROWSER_H
#define LV_100ASK_STM32_TOOL_FILE_BROWSER_H

#ifdef __cplusplus
extern "C" {
#endif


/*********************
 *      INCLUDES
 *********************/
#include "../../lv_100ask.h"


/*********************
 *      DEFINES
 *********************/
#define LV_100ASK_FILE_BROWSER_MAX_BUF     	(128)		// 目录路径最大长度


/**********************
 *      TYPEDEFS
 **********************/
typedef struct _lv_100ask_file_browser {
	lv_obj_t * bg_file_browser;      						// 背景
	lv_obj_t * dir_list;    								// 文件列表句柄
	lv_obj_t * dir_info;    								// 展示目录信息
	char       dir_path[LV_100ASK_FILE_BROWSER_MAX_BUF];	// 目录路径
} T_lv_100ask_file_browser, *PT_lv_100ask_file_browser;


/**********************
 * GLOBAL PROTOTYPES
 **********************/
void lv_100ask_stm32_tool_file_browser(void);





/**********************
 *      MACROS
 **********************/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* LV_100ASK_STM32_TOOL_FILE_BROWSER_H */





