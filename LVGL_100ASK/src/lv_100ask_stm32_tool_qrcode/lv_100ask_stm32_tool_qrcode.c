/**
 ******************************************************************************
 * @file    lv_100ask_stm32_tool_qrcode.c
 * @author  百问科技
 * @version V1.2
 * @date    2020-12-12
 * @brief	二维码创建工具
 ******************************************************************************
 * Change Logs:
 * Date           Author          Notes
 * 2020-12-12     zhouyuebiao     First version
 * 2021-01-25     zhouyuebiao     V1.2 
 ******************************************************************************
 * @attention
 *
 * Copyright (C) 2008-2021 深圳百问网科技有限公司<https://www.100ask.net/>
 * All rights reserved
 *
 ******************************************************************************
 */

/*********************
 *      INCLUDES
 *********************/
#include <stdio.h>
#include <stdlib.h>
#include "lv_100ask_stm32_tool_qrcode.h"
#include "../../lib/lv_lib_qrcode/lv_qrcode.h"


static PT_lv_100ask_qrcode g_pt_lv_100ask_qrcode;  // 结构体


/**********************
 *  STATIC PROTOTYPES
 **********************/
static void lv_100ask_stm32_tool_qrcode_init(void);							// 界面初始化
static void event_handler_ta_cb(lv_obj_t * ta, lv_event_t e);				// 文本框输入触发事件处理函数
static void event_handler_kb_cb(lv_obj_t * kb, lv_event_t e);				// 键盘事件处理函数
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event);	// 返回桌面事件处理函数


/*
 *  函数名：   void lv_100ask_stm32_tool_qrcode(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： 应用初始化入口
*/
void lv_100ask_stm32_tool_qrcode(void)
{
	g_pt_lv_100ask_qrcode = (T_lv_100ask_qrcode *)malloc(sizeof(T_lv_100ask_qrcode));   // 申请内存
	
	g_pt_lv_100ask_qrcode->bg = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_size(g_pt_lv_100ask_qrcode->bg, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_y(g_pt_lv_100ask_qrcode->bg, 0);

    lv_100ask_stm32_tool_qrcode_init();

    add_title(g_pt_lv_100ask_qrcode->bg, "QRCODE");
	add_back(g_pt_lv_100ask_qrcode->bg, event_handler_back_to_home);   // 返回桌面按钮
}



/*
 *  函数名：   static void lv_100ask_stm32_tool_qrcode_init(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： 应用界面初始化
*/
static void lv_100ask_stm32_tool_qrcode_init(void)
{

    /* 创建二维码展示框 */
    const char * str_buffer = "https://www.100ask.net";
    /*Create a 100x100 QR code*/
    g_pt_lv_100ask_qrcode->qr_input = lv_qrcode_create(g_pt_lv_100ask_qrcode->bg, LV_100ASK_QRCODE_SIZE, lv_color_hex3(0x33f), lv_color_hex3(0xeef));
    /*Set data*/
    lv_qrcode_update(g_pt_lv_100ask_qrcode->qr_input, str_buffer, strlen(str_buffer));
    lv_obj_align(g_pt_lv_100ask_qrcode->qr_input, NULL, LV_ALIGN_CENTER, 0, -LV_100ASK_QRCODE_SIZE);  // 摆放位置

    // 创建文本输入框
    g_pt_lv_100ask_qrcode->ta_content = lv_textarea_create(g_pt_lv_100ask_qrcode->bg, NULL);
    lv_obj_set_style_local_text_font(g_pt_lv_100ask_qrcode->ta_content, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_14);  // text font
    lv_obj_set_style_local_value_str(g_pt_lv_100ask_qrcode->ta_content, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, "https://www.100ask.net");    // 设置字符
    lv_obj_set_style_local_value_opa(g_pt_lv_100ask_qrcode->ta_content, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0);       // 设置字符透明度
    lv_obj_set_style_local_radius(g_pt_lv_100ask_qrcode->ta_content, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0); // 设置圆角
    lv_cont_set_fit2(g_pt_lv_100ask_qrcode->ta_content, LV_FIT_PARENT, LV_FIT_NONE);
    lv_textarea_set_text(g_pt_lv_100ask_qrcode->ta_content, "");
    lv_textarea_set_placeholder_text(g_pt_lv_100ask_qrcode->ta_content, "https://www.100ask.net");
    lv_textarea_set_cursor_hidden(g_pt_lv_100ask_qrcode->ta_content, true);
    lv_obj_set_event_cb(g_pt_lv_100ask_qrcode->ta_content, event_handler_ta_cb);
    lv_obj_align(g_pt_lv_100ask_qrcode->ta_content, g_pt_lv_100ask_qrcode->qr_input, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);  // 摆放位置

    /* 下面的仅仅是一些展示信息 */
    /* 展示公司信息 */
    /* LOGO */
    LV_IMG_DECLARE(img_lv_100ask_demo_logo);
    lv_obj_t * logo = lv_img_create(g_pt_lv_100ask_qrcode->bg, NULL);
    lv_img_set_src(logo, &img_lv_100ask_demo_logo);
	lv_obj_align(logo, g_pt_lv_100ask_qrcode->ta_content,  LV_ALIGN_OUT_BOTTOM_LEFT, 15, 40);  //(lv_obj_get_width(logo)/4)


	/* 公司网站信息 */
    lv_obj_t * label_logo = lv_label_create(g_pt_lv_100ask_qrcode->bg, NULL);
	lv_obj_set_style_local_text_font(label_logo, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_14);             // 设置文字大小					    // 设置色彩透明度
    lv_label_set_text(label_logo,  "https://www.100ask.net\nhttp://lvgl.100ask.net");
    lv_obj_align(label_logo, logo, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 0);

    /* 公司二维码信息 */
    /* 官网 */
	lv_obj_t * label_info1 = lv_label_create(g_pt_lv_100ask_qrcode->bg, NULL);
	lv_obj_set_style_local_text_font(label_info1, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_10);
    lv_label_set_text(label_info1, "100ASK");

    /*Create a 100x100 QR code*/
    g_pt_lv_100ask_qrcode->qr_official_website = lv_qrcode_create(g_pt_lv_100ask_qrcode->bg, LV_100ASK_QRCODE_AD_SIZE, LV_COLOR_GREEN, lv_color_hex3(0xeef));
    /*Set data*/
    lv_qrcode_update(g_pt_lv_100ask_qrcode->qr_official_website, "https://www.100ask.net", strlen("https://www.100ask.net"));
    lv_obj_align(g_pt_lv_100ask_qrcode->qr_official_website, NULL, LV_ALIGN_IN_BOTTOM_LEFT, (LV_100ASK_QRCODE_SPACE + (LV_100ASK_QRCODE_SIZE * 0)), -lv_obj_get_height(label_info1));  // 摆放位置
    lv_obj_align(label_info1, g_pt_lv_100ask_qrcode->qr_official_website, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);

     /* 微信公众号 */
	lv_obj_t * label_info2 = lv_label_create(g_pt_lv_100ask_qrcode->bg, NULL);
	lv_obj_set_style_local_text_font(label_info2, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_10);
    lv_label_set_text(label_info2, "WeChat");


    /*Create a 100x100 QR code*/
    g_pt_lv_100ask_qrcode->qr_wechat = lv_qrcode_create(g_pt_lv_100ask_qrcode->bg, LV_100ASK_QRCODE_AD_SIZE, LV_COLOR_GREEN, lv_color_hex3(0xeef));
    /*Set data*/
    lv_qrcode_update(g_pt_lv_100ask_qrcode->qr_wechat, "http://weixin.qq.com/r/M0hvd2-EwjK9rSsU9x0h", strlen("http://weixin.qq.com/r/M0hvd2-EwjK9rSsU9x0h"));
    lv_obj_align(g_pt_lv_100ask_qrcode->qr_wechat, NULL, LV_ALIGN_IN_BOTTOM_LEFT, (LV_100ASK_QRCODE_SPACE + (LV_100ASK_QRCODE_SIZE * 1)), -lv_obj_get_height(label_info2));  // 摆放位置
    lv_obj_align(label_info2, g_pt_lv_100ask_qrcode->qr_wechat, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);

     /* 100ASK LVGL */
	lv_obj_t * label_info3 = lv_label_create(g_pt_lv_100ask_qrcode->bg, NULL);
	lv_obj_set_style_local_text_font(label_info3, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_10);
    lv_label_set_text(label_info3, "100ASK LVGL");


    /*Create a 100x100 QR code*/
    g_pt_lv_100ask_qrcode->qr_100ask_lvgl = lv_qrcode_create(g_pt_lv_100ask_qrcode->bg, LV_100ASK_QRCODE_AD_SIZE, LV_COLOR_GREEN, lv_color_hex3(0xeef));
    /*Set data*/
    lv_qrcode_update(g_pt_lv_100ask_qrcode->qr_100ask_lvgl, "http://lvgl.100ask.net", strlen("http://lvgl.100ask.net"));
    lv_obj_align(g_pt_lv_100ask_qrcode->qr_100ask_lvgl, NULL, LV_ALIGN_IN_BOTTOM_LEFT, (LV_100ASK_QRCODE_SPACE + (LV_100ASK_QRCODE_SIZE * 2)), -lv_obj_get_height(label_info3));  // 摆放位置
    lv_obj_align(label_info3, g_pt_lv_100ask_qrcode->qr_100ask_lvgl, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
}


/*
 *  函数名：   static void event_handler_ta_cb(lv_obj_t * ta, lv_event_t e)
 *  输入参数： 触发事件的文本输入框对象
 *  输入参数： 触发的事件类型
 *  返回值：   无
 *  函数作用： 初始化键盘
*/
static void event_handler_ta_cb(lv_obj_t * ta, lv_event_t e)
{
    if(e == LV_EVENT_RELEASED)
    {
        if(g_pt_lv_100ask_qrcode->keyboard == NULL) {
            lv_obj_set_y(g_pt_lv_100ask_qrcode->bg, -((lv_obj_get_height(ta)/2)));
            g_pt_lv_100ask_qrcode->keyboard = lv_keyboard_create(lv_scr_act(), NULL);
            lv_obj_set_event_cb(g_pt_lv_100ask_qrcode->keyboard, event_handler_kb_cb);

            lv_indev_wait_release(lv_indev_get_act());
        }
        lv_textarea_set_cursor_hidden(ta, false);
        lv_keyboard_set_textarea(g_pt_lv_100ask_qrcode->keyboard, ta);
    }
    else if(e == LV_EVENT_DEFOCUSED)
    {
        lv_textarea_set_cursor_hidden(ta, true);
    }
    else if(e == LV_EVENT_VALUE_CHANGED)
    {
        const char * str_buffer = lv_textarea_get_text(ta);  // 获取输入内容
        printf("str_buffer: %s\n\r", str_buffer);

		/*Set data*/
		lv_qrcode_update(g_pt_lv_100ask_qrcode->qr_input, str_buffer, strlen(str_buffer));
    }
	else if(e == LV_EVENT_LONG_PRESSED_REPEAT) /* 当长按时换行继续输入 */
    {
       
        const char  * txt = "\n\nYou can scroll it if the text is long enough.\n";
        static uint16_t i = 0;
        if(txt[i] != '\0')
        {
            lv_textarea_add_char(ta, txt[i]);
            i++;
        }
    }
}


/*
 *  函数名：   static void event_handler_kb_cb(lv_obj_t * kb, lv_event_t e)
 *  输入参数： 触发事件的对象
 *  输入参数： 触发的事件类型
 *  返回值：   无
 *  函数作用： 退出键盘
*/
static void event_handler_kb_cb(lv_obj_t * kb, lv_event_t e)
{
    lv_keyboard_def_event_cb(g_pt_lv_100ask_qrcode->keyboard, e);

    if((e == LV_EVENT_CANCEL) || (e == LV_EVENT_APPLY)) {
        if(g_pt_lv_100ask_qrcode->keyboard) {
            lv_obj_set_y(g_pt_lv_100ask_qrcode->bg, 0);
            lv_obj_del(g_pt_lv_100ask_qrcode->keyboard);
            g_pt_lv_100ask_qrcode->keyboard = NULL;
        }
    }
}


/*
 *  函数名：   static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数： 触发事件的对象
 *  输入参数： 触发的事件类型
 *  返回值：   无
 *  函数作用： 返回桌面事件处理函数
*/
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		if (g_pt_lv_100ask_qrcode->qr_input != NULL)		        lv_qrcode_delete(g_pt_lv_100ask_qrcode->qr_input);				// 删除二维码
        if (g_pt_lv_100ask_qrcode->qr_official_website != NULL)		lv_qrcode_delete(g_pt_lv_100ask_qrcode->qr_official_website);	// 删除二维码
        if (g_pt_lv_100ask_qrcode->qr_wechat != NULL)		        lv_qrcode_delete(g_pt_lv_100ask_qrcode->qr_wechat);				// 删除二维码
        if (g_pt_lv_100ask_qrcode->qr_100ask_lvgl != NULL)			lv_qrcode_delete(g_pt_lv_100ask_qrcode->qr_100ask_lvgl);		// 删除二维码
        if (g_pt_lv_100ask_qrcode->ta_content != NULL)				lv_obj_del(g_pt_lv_100ask_qrcode->ta_content);					// 删除文本输入框
		if (g_pt_lv_100ask_qrcode->bg != NULL)						lv_obj_del(g_pt_lv_100ask_qrcode->bg);							// 删除背景

		/* 释放内存 */
		free(g_pt_lv_100ask_qrcode); 
		
		/* 清空屏幕，返回桌面 */
        lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
        lv_100ask_stm32_demo_home(10);
    }
}
